# Data Sets

A description of the corresponsing data sets can be found in the papers listed below

## Maximum Capture Problem
Haase, K., Müller, S. (2014): A comparison of linear reformulations for multinomial logit choice probabilities in facility location models. European Journal of Operational Research 232(3), 689–691.

## Retail Stores
Müller, S., Haase, K. (2014): Customer segmentation in retail facility location planning. Business Research 7(2), 235-261.

## Health Care
Krohn, R., Müller, S., Haase, K. (2020): Preventive healthcare facility location planning with quality-conscious clients. OR Spectrum 43, 59-87.

## Student Mode Choice
Tscharaktschiew, S., Müller, S. (2021): Ride to the hills, ride to your school: Physical effort and mode choice. Transportation Research D 98, 102983. 

Müller, S., Mejía Dorantes, L., Kersten, E. (2020): Analysis of active school transportation in hilly urban environments: a case study of Dresden. Journal of Transport Geography (88), 102872.

Müller, S., Tscharaktschiew, S., Haase, K. (2008): Travel-to-school mode choice modelling and patterns of school choice in urban areas. Journal of Transport Geography 16(5), 342-357.
